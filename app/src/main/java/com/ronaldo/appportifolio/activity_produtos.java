package com.ronaldo.appportifolio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;


public class activity_produtos extends AppCompatActivity {
    int qtdbanana = 0;
    int qtdlaranja = 0;
    int qtdsuco = 0;
    int qtdleite = 0;
    int qtdqueijo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);

    }

    // seta quantidade para banana
    public void somarbanana(View view){
        qtdbanana = qtdbanana + 1;
        displayQuantidadeBanana(qtdbanana);
    }

    public void subtrairbanana(View view){
        qtdbanana = qtdbanana - 1;
        displayQuantidadeBanana(qtdbanana);
    }

    public void displayQuantidadeBanana(int qtdbananaset){
        TextView qtdbanana = (TextView) findViewById(R.id.edtqtdbanana);
        qtdbanana.setText("" + qtdbananaset);
    }
    //--------------------------------------------------------------
    // seta quantidade para laranja
    public void somarlaranja(View view){
        qtdlaranja = qtdlaranja + 1;
        displayQuantidadeLaranja(qtdlaranja);
    }

    public void subtrairlaranja(View view){
        qtdlaranja = qtdlaranja - 1;
        displayQuantidadeLaranja(qtdbanana);
    }

    public void displayQuantidadeLaranja(int qtdlaranjaset){
        TextView qtdlaranja= (TextView) findViewById(R.id.edtqtdlaranja);
        qtdlaranja.setText("" + qtdlaranjaset);
    }
    //--------------------------------------------------------------
    // seta quantidade para suco de laranja
    public void somarsuco(View view){
        qtdsuco = qtdsuco + 1;
        displayQuantidadeSuco(qtdsuco);
    }

    public void subtrairsuco(View view){
        qtdsuco = qtdsuco - 1;
        displayQuantidadeSuco(qtdsuco);
    }

    public void displayQuantidadeSuco(int qtdsucoset){
        TextView qtdsuco= (TextView) findViewById(R.id.edtqtdsuco);
        qtdsuco.setText("" + qtdsucoset);
    }
    //--------------------------------------------------------------
    // seta quantidade para suco de leite
    public void somarleite(View view){
        qtdleite = qtdleite + 1;
        displayQuantidadeLeite(qtdleite);
    }

    public void subtrairleite(View view){
        qtdleite = qtdleite - 1;
        displayQuantidadeLeite(qtdleite);
    }

    public void displayQuantidadeLeite(int qtdleiteset){
        TextView qtdleite= (TextView) findViewById(R.id.edtqtdleite);
        qtdleite.setText("" + qtdleiteset);
    }
    //--------------------------------------------------------------
    // seta quantidade para suco de queijo
    public void somarqueijo(View view){
        qtdqueijo = qtdqueijo + 1;
        displayQuantidadeQueijo(qtdqueijo);
    }

    public void subtrairqueijo(View view){
        qtdqueijo = qtdqueijo - 1;
        displayQuantidadeQueijo(qtdqueijo);
    }

    public void displayQuantidadeQueijo(int qtdqueijoset){
        TextView qtdqueijo= (TextView) findViewById(R.id.edtqtdqueijo);
        qtdqueijo.setText("" + qtdqueijoset);
    }

    public int calculaPedido (int total){
        int cbanana = 7 * qtdbanana;
        int claranja = 12 * qtdlaranja;
        int csuco = 5 * qtdsuco;
        int cleite = 3 * qtdleite;
        int cqueijo = 30 * qtdqueijo;

        return cbanana + claranja + csuco + cleite + cqueijo;

    }


    // botao revisar carrinho abrir activity do carrinho de compras.
    public void abrirCarrinho(View view){

        Intent intent = new Intent(this, activity_carrinho.class);
       startActivity(intent);
    }




}