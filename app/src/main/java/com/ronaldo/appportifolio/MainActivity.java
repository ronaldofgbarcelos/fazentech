package com.ronaldo.appportifolio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnlogin = (Button) findViewById(R.id.btnlogin);
        Button btncaduser = (Button) findViewById(R.id.btncaduser);

        btncaduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, activity_caduser.class));
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtusername = (TextView) findViewById(R.id.txtusername);
                TextView txtsenha = (TextView) findViewById(R.id.txtsenha);
                String login = txtusername.getText().toString();
                String senha = txtsenha.getText().toString();

                if (login.equals("ronaldo")&&senha.equals("internacional")){
                    alert ("Bem vindo " + login);
                    startActivity(new Intent(MainActivity.this, activity_produtos.class));

                }else{
                    alert ("Usuário ou senha inválidos");
                }

            }

        });
    }
    private void alert (String s){
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();

    }
}